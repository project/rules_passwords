INTRODUCTION
------------

The Rules Passwords module provides actions for the Rules module that allow
you to deal with user passwords.

The following actions are provided:

 * Add a password (under "Data" group) - add a text variable containing a
   password to the current Rules context. You can use the standard Drupal
   password generating function, or use a custom function which can generate
   passwords from a customizable set of characters.

 * Set password (under "User" group) - set the password for a user.


REQUIREMENTS
------------

This module requires no extra module (besides Rules, of course).


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7 for
   further information.


CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no
configuration. When enabled, the module will provide the actions described
above to the Rules module automatically.
