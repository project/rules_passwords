<?php
/**
 * @file
 * Rules hooks for the Rules Passwords module.
 */

/**
 * Implements hook_rules_action_info().
 */
function rules_passwords_rules_action_info() {
  return array(
    'rules_passwords_add_password' => array(
      'label' => t('Add a password'),
      'group' => t('Data'),
      'parameter' => array(
        'length' => array(
          'type' => 'integer',
          'label' => t('Length'),
          'default value' => 10,
          'description' => t('The password length'),
        ),
        'generator' => array(
          'type' => 'integer',
          'label' => t('Generator'),
          'description' => t('The generator function to be used for creating the password'),
          'options list' => '_rules_passwords_generators_list',
        ),
        'set' => array(
          'type' => 'text',
          'label' => t('Characters'),
          'description' => t('The set of characters to choose from and generate the password. <strong>Only used by the "Custom" generator</strong>. Leave empty for mixed-case letters, numbers, and symbols.'),
          'optional' => TRUE,
        ),
      ),
      'provides' => array(
        'password' => array(
          'label' => t('Password'),
          'type' => 'text',
        ),
      ),
    ),
    'rules_passwords_set_user_password' => array(
      'label' => t('Set password'),
      'group' => t('User'),
      'parameter' => array(
        'user' => array(
          'label' => t('User'),
          'type' => 'user',
          'save' => TRUE,
        ),
        'password' => array(
          'label' => t('Password'),
          'type' => 'text',
          'default mode' => 'selector',
        ),
      ),
    ),
  );
}
